package com.gremlin;

import com.gremlin.utils.RedisUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class RedisApplicationTests {

    @Autowired
    private RedisUtil redisUtil;

    @Test
    public void test01() {
        // redisUtil.setString("string","String测试");
        // Map<String,Object> map = new HashMap<>();
        // map.put("3","StringMap测试1");
        // map.put("4","StringMap测试3");
        // redisUtil.setStringMultiIfAbsent(map);
        // System.out.println(redisUtil.hasKey("string"));
        // redisUtil.delKey("1","2","3");
        // redisUtil.setHash("hash","1","StringMap测试1");
        // redisUtil.setHash("hash","2","StringMap测试2");
        // System.out.println(redisUtil.getHash("hash","1"));
        // redisUtil.setHashMap("hash",map);
        // System.out.println(redisUtil.hasHashKey("hash","9"));
        // System.out.println(redisUtil.delHash("hash","1","2"));
        // System.out.println(redisUtil.hincr("hash","4",5));
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        redisUtil.setList("list",list);
        System.out.println(redisUtil.getList("list"));
        System.out.println(redisUtil.getListSize("list"));
        System.out.println(redisUtil.getListByIndex("list",1));
        redisUtil.updateListByIndex("list",0,99999);
        // System.out.println(redisUtil.delList("list",2,"[1,2,3]"));
        // redisUtil.setSet("set",1);
        // redisUtil.setSet("set",2);
        // redisUtil.setSet("set",3,4,5);
        // System.out.println(redisUtil.getSet("set"));
        // System.out.println(redisUtil.hasSetValue("set",9));
        // System.out.println(redisUtil.getSetSize("set"));
        // System.out.println(redisUtil.delSet("set",3));
        // System.out.println(redisUtil.getSetSize("set"));
    }

}
