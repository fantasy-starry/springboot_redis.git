// package com.gremlin.handel;
//
// import lombok.extern.slf4j.Slf4j;
//
// /**
//  * @author
//  * @date 2022/9/23 10:28
//  */
// @Aspect
// @Slf4j
// public class RedisAspect {
//     @Pointcut("execution(* com.study.springboot.common.utils.*(..))")
//     public void pointcut(){
//     }
//     @Around("pointcut()")
//     public Object handleException(ProceedingJoinPoint joinPoint){
//         Object result = null;
//         try {
//             result= joinPoint.proceed();
//         } catch (Throwable throwable) {
//             log.error("redis may be some wrong");
//         }
//         return result;
//     }
//
// }